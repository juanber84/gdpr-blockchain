const { host, port } = require('./../parameters').ganache

module.exports = {
  networks: {
    development: {
      host,
      port,
      network_id: "5777" // Match any network id
    // optional config values:
    // gas
    // gasPrice
    // from - default address to use for any transaction Truffle makes during migrations
    // provider - web3 provider instance Truffle should use to talk to the Ethereum network.
    //          - function that returns a web3 provider instance (see below.)
    //          - if specified, host and port are ignored.
    }
  }
}