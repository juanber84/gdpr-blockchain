const { host, port } = require('./../../parameters').ganache

const Web3 = require('web3')
const co = require('co')

var provider = new Web3.providers.HttpProvider(`http://${host}:${port}`);
var contract = require("truffle-contract");
var SimpleStorage = require('./../build/contracts/SimpleStorage.json')

var MyContract = contract(SimpleStorage)

console.log(`http://${host}:${port}`)

MyContract.setProvider(provider)

function fixTruffleContractCompatibilityIssue(contract) {
    if (typeof contract.currentProvider.sendAsync !== "function") {
        contract.currentProvider.sendAsync = function () {
            return contract.currentProvider.send.apply(
                contract.currentProvider, arguments
            );
        };
    }
    return contract;
}

co(function* () {

    let a = fixTruffleContractCompatibilityIssue(MyContract) 

    yield a.deployed()
        .then(function (instance) {
            console.log('.......')
            console.log('.......')
            console.log('.......')
            console.log('.......')
            console.log('.......')
            var deployed = instance;
            return instance.someFunction(5);
        }).then(function (result) {
            // Do something with the result or continue with more transactions.
        })
        .catch(err => {
            console.log(err)
        })
})
